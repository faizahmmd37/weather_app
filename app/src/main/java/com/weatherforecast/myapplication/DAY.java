package com.weatherforecast.myapplication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DAY {

    @SerializedName("condition")
    @Expose
    private CONDITION condition;

    @SerializedName("maxtemp_c")
    @Expose
    private double maxtemp_c;

    @SerializedName("maxtemp_f")
    @Expose
    private double maxtemp_f;

    @SerializedName("mintemp_c")
    @Expose
    private double mintemp_c;

    @SerializedName("mintemp_f")
    @Expose
    private double mintemp_f;

    @SerializedName("avgtemp_c")
    @Expose
    private double avgtemp_c;

    @SerializedName("avgtemp_f")
    @Expose
    private double avgtemp_f;

    @SerializedName("maxwind_mph")
    @Expose
    private double maxwind_mph;

    @SerializedName("maxwind_kph")
    @Expose
    private double maxwind_kph;

    @SerializedName("totalprecip_mm")
    @Expose
    private double totalprecip_mm;

    @SerializedName("totalprecip_in")
    @Expose
    private double totalprecip_in;

    @SerializedName("avgvis_km")
    @Expose
    private double avgvis_km;

    @SerializedName("avgvis_miles")
    @Expose
    private double avgvis_miles;

    @SerializedName("avghumidity")
    @Expose
    private double avghumidity;

    @SerializedName("daily_will_it_rain")
    @Expose
    private int daily_will_it_rain;

    @SerializedName("daily_chance_of_rain")
    @Expose
    private String daily_chance_of_rain;

    @SerializedName("daily_will_it_snow")
    @Expose
    private int daily_will_it_snow;

    @SerializedName("daily_chance_of_snow")
    @Expose
    private String daily_chance_of_snow;

    @SerializedName("uv")
    @Expose
    private double uv;

    public double getMaxtemp_c() {
        return maxtemp_c;
    }

    public void setMaxtemp_c(double maxtemp_c) {
        this.maxtemp_c = maxtemp_c;
    }

    public double getMaxtemp_f() {
        return maxtemp_f;
    }

    public void setMaxtemp_f(double maxtemp_f) {
        this.maxtemp_f = maxtemp_f;
    }

    public double getMintemp_c() {
        return mintemp_c;
    }

    public void setMintemp_c(double mintemp_c) {
        this.mintemp_c = mintemp_c;
    }

    public double getMintemp_f() {
        return mintemp_f;
    }

    public void setMintemp_f(double mintemp_f) {
        this.mintemp_f = mintemp_f;
    }

    public double getAvgtemp_c() {
        return avgtemp_c;
    }

    public void setAvgtemp_c(double avgtemp_c) {
        this.avgtemp_c = avgtemp_c;
    }

    public double getAvgtemp_f() {
        return avgtemp_f;
    }

    public void setAvgtemp_f(double avgtemp_f) {
        this.avgtemp_f = avgtemp_f;
    }

    public double getMaxwind_mph() {
        return maxwind_mph;
    }

    public void setMaxwind_mph(double maxwind_mph) {
        this.maxwind_mph = maxwind_mph;
    }

    public double getMaxwind_kph() {
        return maxwind_kph;
    }

    public void setMaxwind_kph(double maxwind_kph) {
        this.maxwind_kph = maxwind_kph;
    }

    public double getTotalprecip_mm() {
        return totalprecip_mm;
    }

    public void setTotalprecip_mm(double totalprecip_mm) {
        this.totalprecip_mm = totalprecip_mm;
    }

    public double getTotalprecip_in() {
        return totalprecip_in;
    }

    public void setTotalprecip_in(double totalprecip_in) {
        this.totalprecip_in = totalprecip_in;
    }

    public double getAvgvis_km() {
        return avgvis_km;
    }

    public void setAvgvis_km(double avgvis_km) {
        this.avgvis_km = avgvis_km;
    }

    public double getAvgvis_miles() {
        return avgvis_miles;
    }

    public void setAvgvis_miles(double avgvis_miles) {
        this.avgvis_miles = avgvis_miles;
    }

    public double getAvghumidity() {
        return avghumidity;
    }

    public void setAvghumidity(double avghumidity) {
        this.avghumidity = avghumidity;
    }

    public int getDaily_will_it_rain() {
        return daily_will_it_rain;
    }

    public void setDaily_will_it_rain(int daily_will_it_rain) {
        this.daily_will_it_rain = daily_will_it_rain;
    }

    public String getDaily_chance_of_rain() {
        return daily_chance_of_rain;
    }

    public void setDaily_chance_of_rain(String daily_chance_of_rain) {
        this.daily_chance_of_rain = daily_chance_of_rain;
    }

    public int getDaily_will_it_snow() {
        return daily_will_it_snow;
    }

    public void setDaily_will_it_snow(int daily_will_it_snow) {
        this.daily_will_it_snow = daily_will_it_snow;
    }

    public String getDaily_chance_of_snow() {
        return daily_chance_of_snow;
    }

    public void setDaily_chance_of_snow(String daily_chance_of_snow) {
        this.daily_chance_of_snow = daily_chance_of_snow;
    }

    public double getUv() {
        return uv;
    }

    public void setUv(double uv) {
        this.uv = uv;
    }

    public CONDITION getCondition() {
        return condition;
    }

    public void setCondition(CONDITION condition) {
        this.condition = condition;
    }
}
