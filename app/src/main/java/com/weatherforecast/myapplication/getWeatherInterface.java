package com.weatherforecast.myapplication;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface getWeatherInterface {
@GET("forecast.json?key=e1f63a69178a47e8bb1154622200509&q&days=7")
Call<ROOT> getweatherMethod(@Query("q") String latlon);

}
