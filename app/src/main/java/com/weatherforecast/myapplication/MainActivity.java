package com.weatherforecast.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.format.Formatter;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.internal.DescendantOffsetUtils;
import com.google.android.material.navigation.NavigationView;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.logging.Handler;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    String P, A, B, C, D, E, F, EE, FF;
    double AA, BB, CC, DD;
    AlertDialog.Builder alert;
    NavigationView navi;
    DrawerLayout drawerLayout;
    Toolbar toolbar;
    TextView toolbartitle, dateandtime, txt2, txt1, txt_max_temp, txt_min_temp, txt_wind, txt_uv, txt_pressure, txt_humidity, tomorrow_date;
    TextView dayaftertomorrowdate, txt3, txt_wind2, txt_avgtemp2, txt_avghumidity2, sunrisetime, sunsettime, moonrisetime, moonsettime;
    TextView txt4, txt_wind3, txt_avgtemp3, txt_avghumidity3, sunrisetime2, sunsettime2, moonrisetime2, moonsettime2;
    ImageView img1, img2, img3;
    String current_weather_img, tomorrow_weather_img, dayaftertomorrow_weather_img;
    Handler handler;

    String last;
    String latlonsplash;
    LocationManager locationManager;
    Location location;
    Double LAT, LONG;
    ProgressBar progressBar;
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        navi = findViewById(R.id.nav);
        drawerLayout = findViewById(R.id.drawerid);
        toolbartitle = findViewById(R.id.toolbar_title);
        dateandtime = findViewById(R.id.dateandtime);
        txt2 = findViewById(R.id.txt2);
        txt1 = findViewById(R.id.txt1);
        txt_max_temp = findViewById(R.id.text_max_temp);
        txt_min_temp = findViewById(R.id.text_min_temp);
        txt_wind = findViewById(R.id.txt_wind);
        txt_uv = findViewById(R.id.txt_uv);
        txt_pressure = findViewById(R.id.txt_pressure);
        txt_humidity = findViewById(R.id.txt_humidity);
        tomorrow_date = findViewById(R.id.tomorrowdate);
        dayaftertomorrowdate = findViewById(R.id.dayaftertomorrowdate);
        txt3 = findViewById(R.id.txt3);
        txt_wind2 = findViewById(R.id.txt_wind2);
        txt_avgtemp2 = findViewById(R.id.txt_avgtemp2);
        txt_avghumidity2 = findViewById(R.id.txt_avghumidity2);
        sunsettime = findViewById(R.id.sunsettime);
        sunrisetime = findViewById(R.id.sunrisetime);
        moonrisetime = findViewById(R.id.moonrisetime);
        moonsettime = findViewById(R.id.moonsettime);
        txt4 = findViewById(R.id.txt4);
        txt_wind3 = findViewById(R.id.txt_wind3);
        txt_avgtemp3 = findViewById(R.id.txt_avgtemp3);
        txt_avghumidity3 = findViewById(R.id.txt_avghumidity3);
        sunrisetime2 = findViewById(R.id.sunrisetime2);
        sunsettime2 = findViewById(R.id.sunsettime2);
        moonrisetime2 = findViewById(R.id.moonrisetime2);
        moonsettime2 = findViewById(R.id.moonsettime2);
        img1 = findViewById(R.id.img1);
        img2 = findViewById(R.id.img2);
        img3 = findViewById(R.id.img3);
        progressBar = findViewById(R.id.progressBar_cyclic);
        swipeRefreshLayout = findViewById(R.id.swiperefresh_layout);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(MainActivity.this, drawerLayout, toolbar, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {


            try {
                last = new myAsync().execute().get();

            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (LAT != null && LONG != null) {
               new myAsync().cancel(true);
                callRetrofit(LAT + "," + LONG);

            } else if (LAT == null || LONG == null) {
                Toast.makeText(MainActivity.this, "unable to find location", Toast.LENGTH_SHORT).show();

            }

        } else {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 37);

            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 37);
            }

        }


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                try {
                    last = new myAsync().execute().get();

                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                //Toast.makeText(MainActivity.this, LAT+","+LONG, Toast.LENGTH_SHORT).show();

                if (LAT != null && LONG != null) {
                   new myAsync().cancel(true);
                    callRetrofit(LAT + "," + LONG);
                } else if (LAT == null || LONG == null) {
                    Toast.makeText(MainActivity.this, "unable to find location", Toast.LENGTH_SHORT).show();
                }
            }


        });

        navi.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.aboutitem:
                        AlertDialog.Builder alertabout = new AlertDialog.Builder(MainActivity.this);
                        alertabout.setMessage(R.string.aboutus);
                        alertabout.setCancelable(true);
                        alertabout.setTitle("About us");
                        alertabout.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        alertabout.create();
                        alertabout.show();


                        break;
                    case R.id.termsofserviceitem:
                        AlertDialog.Builder alerttermsofservice = new AlertDialog.Builder(MainActivity.this);
                        alerttermsofservice.setMessage(R.string.termsofservice);
                        alerttermsofservice.setCancelable(true);
                        alerttermsofservice.setTitle("Terms of service");
                        alerttermsofservice.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        alerttermsofservice.create();
                        alerttermsofservice.show();
                        break;
                    case R.id.shareitem:
                        Intent intentshare = new Intent();
                        intentshare.setAction(Intent.ACTION_SEND);
                        final String apppackagename = "com.weatherforecast.myapplication";
                        intentshare.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=" + apppackagename);
                        intentshare.setType("text/plain");
                        Intent.createChooser(intentshare, "Share via");
                        startActivity(intentshare);
                        break;
                    case R.id.rateus:
                        final String packagename = "com.weatherforecast.myapplication";
                        Uri uri = Uri.parse("market://details?id=" + packagename);
                        Intent intentrate = new Intent(Intent.ACTION_VIEW, uri);
                        try {
                            startActivity(intentrate);
                        } catch (ActivityNotFoundException e) {
                            Toast.makeText(MainActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();
                        }

                        break;
                    case R.id.contactitem:
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_SEND);
                        String[] recipients = {"faizahmmd37@gmail.com"};
                        String[] cc_recipient = {"nightowlstudioz@gmail.com"};
                        intent.putExtra(Intent.EXTRA_CC, cc_recipient);
                        intent.putExtra(Intent.EXTRA_EMAIL, recipients);
                        intent.putExtra(Intent.EXTRA_SUBJECT, "Contact us form");
                        intent.putExtra(Intent.EXTRA_TEXT, "Write your message here...");

                        intent.setType("text/html");
                        intent.setPackage("com.google.android.gm");
                        startActivity(Intent.createChooser(intent, "send email"));
                        break;
                }
                drawerLayout.closeDrawer(GravityCompat.START);
                return false;
            }
        });

    }


    private class DownloadImage extends AsyncTask<String, Integer, List<Bitmap>> {
        HttpURLConnection httpURLConnection = null;
        InputStream inputStream;

        @Override
        protected List<Bitmap> doInBackground(String... strings) {
            int count = strings.length;
            List<Bitmap> bitmaps = new ArrayList<>();
            for (int i = 0; i < count; i++) {

                String imageurl = strings[i];

                try {
                    java.net.URL url = new java.net.URL(imageurl);
                    httpURLConnection = (HttpURLConnection) url.openConnection();
                    httpURLConnection.connect();
                    inputStream = httpURLConnection.getInputStream();
                    BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
                    Bitmap bitmap = BitmapFactory.decodeStream(bufferedInputStream);
                    bitmaps.add(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                }
                httpURLConnection.disconnect();
                ;
            }

            return bitmaps;
        }

        @Override
        protected void onPostExecute(List<Bitmap> bitmaps) {
            super.onPostExecute(bitmaps);
            Bitmap bitmap1 = bitmaps.get(0);
            Bitmap bitmap2 = bitmaps.get(1);
            Bitmap bitmap3 = bitmaps.get(2);

            img1.setImageBitmap(bitmap1);
            img2.setImageBitmap(bitmap2);
            img3.setImageBitmap(bitmap3);

        }
    }

    public void callRetrofit(String cityretrofitcall) {

        Retrofit retrofit = new Retrofit.Builder().baseUrl("http://api.weatherapi.com/v1/").addConverterFactory(GsonConverterFactory.create()).build();
        getWeatherInterface getweatherinterface = retrofit.create(com.weatherforecast.myapplication.getWeatherInterface.class);

        Call<ROOT> call = getweatherinterface.getweatherMethod(cityretrofitcall);

        call.enqueue(new Callback<ROOT>() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onResponse(Call<ROOT> call, Response<ROOT> response) {
                if (response.body() != null) {
                    if (swipeRefreshLayout.isRefreshing()) {
                        Toast.makeText(MainActivity.this, "refreshed", Toast.LENGTH_SHORT).show();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                    ROOT root = response.body();
                    String locality = root.getLocation().getName();
                    String region = root.getLocation().getRegion();
                    String country = root.getLocation().getCountry();
                    String time = root.getLocation().getLocaltime();
                    double latitude = root.getLocation().getLat();
                    double longitude = root.getLocation().getLon();
                    String lastupdated = root.getCurrent().getLastupdated();
                    double temp_c = root.getCurrent().getTemp_c();
                    double temp_f = root.getCurrent().getTemp_f();
                    double temp_max = root.getForecast().getForecastday().get(0).getDay().getMaxtemp_c();
                    double temp_min = root.getForecast().getForecastday().get(0).getDay().getMintemp_c();
                    double wind_mph = root.getCurrent().getWind_mph();
                    double wind_kph = root.getCurrent().getWind_kph();
                    int wind_degree = root.getCurrent().getWind_degree();
                    String wind_direction = root.getCurrent().getWind_dir();
                    double pressure_mb = root.getCurrent().getPressure_mb();
                    double pressure_in = root.getCurrent().getPressure_in();
                    int humidity = root.getCurrent().getHumidity();
                    double uv = root.getCurrent().getUv();
                    String weathercondition = root.getCurrent().getConditioncurrent().getText().toString();
                    String tomorrow_date_value = root.getForecast().getForecastday().get(1).getDate().toString();
                    String dayaftertomorrow_value = root.getForecast().getForecastday().get(2).getDate().toString();
                    String tomorrow_condition = root.getForecast().getForecastday().get(1).getDay().getCondition().getText().toString();
                    double tomorrow_wind = root.getForecast().getForecastday().get(1).getDay().getMaxwind_kph();
                    double tomorrow_avgtemp = root.getForecast().getForecastday().get(1).getDay().getAvgtemp_c();
                    double tomorrow_avghumidity = root.getForecast().getForecastday().get(1).getDay().getAvghumidity();
                    String tomorrow_sunrise = root.getForecast().getForecastday().get(1).getAstro().getSunrise();
                    String tomorrow_sunset = root.getForecast().getForecastday().get(1).getAstro().getSunset();
                    String tomorrow_moonrise = root.getForecast().getForecastday().get(1).getAstro().getMoonrise();
                    String tomorrow_moonset = root.getForecast().getForecastday().get(1).getAstro().getMoonset();

                    String dayaftertomorrow_condition = root.getForecast().getForecastday().get(2).getDay().getCondition().getText().toString();
                    double dayaftertomorrow_wind = root.getForecast().getForecastday().get(2).getDay().getMaxwind_kph();
                    double dayaftertomorrow_avgtemp = root.getForecast().getForecastday().get(2).getDay().getAvgtemp_c();
                    double dayaftertomorrow_avghumidity = root.getForecast().getForecastday().get(2).getDay().getAvghumidity();
                    String dayaftertomorrow_sunrise = root.getForecast().getForecastday().get(2).getAstro().getSunrise();
                    String dayaftertomorrow_sunset = root.getForecast().getForecastday().get(2).getAstro().getSunset();
                    String dayaftertomorrow_moonrise = root.getForecast().getForecastday().get(2).getAstro().getMoonrise();
                    String dayaftertomorrow_moonset = root.getForecast().getForecastday().get(2).getAstro().getMoonset();

                    current_weather_img = root.getCurrent().getConditioncurrent().getIcon();

                    tomorrow_weather_img = root.getForecast().getForecastday().get(1).getDay().getCondition().getIcon();
                    dayaftertomorrow_weather_img = root.getForecast().getForecastday().get(2).getDay().getCondition().getIcon();

                    //for image downloading
                    String url1 = "http:" + current_weather_img;
                    String url2 = "http:" + tomorrow_weather_img;
                    String url3 = "http:" + dayaftertomorrow_weather_img;
                    new DownloadImage().execute(url1, url2, url3);


                    int Size = root.getForecast().getForecastday().size();

                    int forbackgrnd = root.getCurrent().getIs_day();
                    if (forbackgrnd == 1) {
                        drawerLayout.setBackgroundResource(R.drawable.background3);
                    } else {

                        drawerLayout.setBackgroundResource(R.drawable.background);
                    }

                    toolbartitle.setText(last+","+region);
                    dateandtime.setText(time);
                    txt2.setText(weathercondition);
                    txt1.setText(temp_c + "");
                    txt_max_temp.setText(temp_max + "\u2103");
                    txt_min_temp.setText(temp_min + "\u2103");
                    txt_wind.setText(wind_kph + "km/h, " + wind_degree + "\u00B0" + wind_direction);
                    txt_uv.setText(uv + "");
                    txt_pressure.setText(pressure_mb + " mb");
                    txt_humidity.setText(humidity + "");
                    tomorrow_date.setText(tomorrow_date_value);
                    dayaftertomorrowdate.setText(dayaftertomorrow_value);
                    txt3.setText(tomorrow_condition);
                    txt_wind2.setText(tomorrow_wind + " km/h");
                    txt_avgtemp2.setText(tomorrow_avgtemp + "\u2103");
                    txt_avghumidity2.setText(tomorrow_avghumidity + "");
                    sunsettime.setText(tomorrow_sunset);
                    sunrisetime.setText(tomorrow_sunrise);
                    moonrisetime.setText(tomorrow_moonrise);
                    moonsettime.setText(tomorrow_moonset);
                    txt4.setText(dayaftertomorrow_condition);
                    txt_wind3.setText(dayaftertomorrow_wind + " km/h");
                    txt_avgtemp3.setText(dayaftertomorrow_avgtemp + " \u2103");
                    txt_avghumidity3.setText(dayaftertomorrow_avghumidity + "");
                    sunrisetime2.setText(dayaftertomorrow_sunrise);
                    sunsettime2.setText(dayaftertomorrow_sunset);
                    moonrisetime2.setText(dayaftertomorrow_moonrise);
                    moonsettime2.setText(dayaftertomorrow_moonset);


                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ROOT> call, Throwable t) {
                Toast.makeText(MainActivity.this, t+"", Toast.LENGTH_LONG).show();
            }
        });


    }

    public class myAsync extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... voids) {


            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            }

            if (location != null) {
                LAT = location.getLatitude();
                LONG = location.getLongitude();

                Geocoder geocoder = new Geocoder(MainActivity.this);
                try {
                    List<Address> list = geocoder.getFromLocation(LAT, LONG, 1);

                    latlonsplash =list.get(0).getLocality();

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            return latlonsplash;
        }

    }

    @Override
    public void onBackPressed() {
        CallAlertBAR();

    }

    public void CallAlertBAR() {
        AlertDialog.Builder alertBar = new AlertDialog.Builder(this);
        alertBar.setTitle("EXIT");
        alertBar.setMessage("Do you really want to exit?");
        alertBar.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alertBar.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                // intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                System.exit(1);
            }
        });
        alertBar.create();
        alertBar.show();
    }
}