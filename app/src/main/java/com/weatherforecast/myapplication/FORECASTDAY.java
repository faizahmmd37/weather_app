package com.weatherforecast.myapplication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FORECASTDAY {

    @SerializedName("day")
    @Expose
    private DAY day;

    @SerializedName("astro")
    @Expose
    private ASTRO astro;


    @SerializedName("date")
    @Expose
    private String date;

    public ASTRO getAstro() {
        return astro;
    }

    public void setAstro(ASTRO astro) {
        this.astro = astro;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public DAY getDay() {
        return day;
    }

    public void setDay(DAY day) {
        this.day = day;
    }
}
