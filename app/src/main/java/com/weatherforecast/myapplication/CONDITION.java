package com.weatherforecast.myapplication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CONDITION {

    @SerializedName("text")
    @Expose
    private String text;

   @SerializedName("icon")
    @Expose
   private String icon;

    public String getIcon() {
       return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
