package com.weatherforecast.myapplication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ROOT {
    @SerializedName("location")
    @Expose
    private LOCATION location;

    @SerializedName("current")
    @Expose
    private CURRENT current;

    @SerializedName("forecast")
    @Expose
    private FORECAST forecast;

    public CURRENT getCurrent() {
        return current;
    }

    public void setCurrent(CURRENT current) {
        this.current = current;
    }

    public FORECAST getForecast() {
        return forecast;
    }

    public void setForecast(FORECAST forecast) {
        this.forecast = forecast;
    }

    public LOCATION getLocation() {
        return location;
    }

    public void setLocation(LOCATION location) {
        this.location = location;
    }
}
