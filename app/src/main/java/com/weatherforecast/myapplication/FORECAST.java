package com.weatherforecast.myapplication;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FORECAST {

    @SerializedName("forecastday")
    @Expose
    private ArrayList<FORECASTDAY> forecastday;

    public ArrayList<FORECASTDAY> getForecastday() {
        return forecastday;
    }

    public void setForecastday(ArrayList<FORECASTDAY> forecastday) {
        this.forecastday = forecastday;
    }
}
